#!/usr/bin/env bash

state=$(eww get open-network-list)

open-network-list() {
    eww update open-network-list=true
}

close-network-list() {
    eww update open-network-list=false
}

case $1 in
    close)
        close-network-list
        exit 0;;
esac

case $state in
    true)
        close-network-list;;
    false)
        open-network-list;;
esac
