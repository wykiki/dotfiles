#!/usr/bin/env bash

function get_networks {
  local connectivity=$(nmcli networking connectivity)
  local ssid=$(nmcli -t -f NAME connection show --active | head -n1 | tr -d '\\' | sed 's/\"/\\"/g')
  local signal=$(nmcli -f in-use,signal dev wifi | rg "\*" | awk '{ print $2 }')
  local json=$(jq \
    -n \
    --arg connectivity "$connectivity" \
    --arg ssid "$ssid" \
    --arg signal "$signal" \
    '{
      "connectivity": $connectivity,
      "ssid": $ssid,
      "signal": $signal
    }'
  )
  echo $json
}

get_networks

ip monitor link | while read -r line; do
  get_networks
done
