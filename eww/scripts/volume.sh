#!/usr/bin/env bash

function muted {
  local type=$1
  pactl get-$type-mute @DEFAULT_${type^^}@ | rg -q 'yes' && echo 'muted'
}

function volume {
  local type=$1
  arr="$(pactl get-$type-volume @DEFAULT_${type^^}@)"
  for token in $arr; do
    echo $token
  done | rg '%' | head -n 1 | tr -d '%'
}

TYPE=""

case $1 in
  out)
    TYPE=sink
    ;;
  in)
    TYPE=source
    ;;
esac

muted $TYPE || volume $TYPE

pactl subscribe | rg --line-buffered "on $TYPE" | while read -r _; do
  muted $TYPE || volume $TYPE
done
