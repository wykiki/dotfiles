export extern main [
  context?: string@complete-kubens-context
]

def complete-kubens-context [] {
  ^kubens | lines
}
