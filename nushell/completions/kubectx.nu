export extern main [
  context?: string@complete-kubectx-context
]

def complete-kubectx-context [] {
  ^kubectx | lines
}
