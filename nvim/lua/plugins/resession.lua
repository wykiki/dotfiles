return {
  "stevearc/resession.nvim",
  opts = {
    autosave = {
      enabled = true,
      interval = 60,
      notify = true,
    },
  },
}
