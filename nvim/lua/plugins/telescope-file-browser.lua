return {
  "nvim-telescope/telescope.nvim",
  dependencies = {
    {
      "AstroNvim/astrocore",
      opts = {
        mappings = {
          n = {
            ["<Leader>fe"] = { "<Cmd>Telescope file_browser path=%:p:h select_buffer=true<CR>", desc = "Open File browser" },
          },
        },
      },
    },
  },
}
