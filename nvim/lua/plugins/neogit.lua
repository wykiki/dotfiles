return {
  "NeogitOrg/neogit",
  dependencies = {
    {
      "AstroNvim/astrocore",
      opts = {
        mappings = {
          n = {
            ["<Leader>nn"] = { function() require("neogit").open({kind = "floating"}) end, desc = "Open Neogit floating" },
          },
        },
      },
    },
  },
}
