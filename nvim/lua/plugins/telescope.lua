return {
  "nvim-telescope/telescope.nvim",
  opts = {
    defaults = {
      file_ignore_patterns = {
        -- Git
        ".git/",
        -- Rust
        "target/",
        -- Angular
        ".angular/",
        ".nx/",
        "dist/",
        "node_modules/",
      },
    },
    pickers = {
      find_files = {
        hidden = true
      },
    },
  },
}
