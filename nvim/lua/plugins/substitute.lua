return {
  "gbprod/substitute.nvim",
  opts = {},
  dependencies = {
    "AstroNvim/astrocore",
    opts = {
      mappings = {
        n = {
          ["<leader>y"] = { function() require("substitute").operator() end, desc = "Substitute motion" },
          ["<leader>yy"] = { function() require("substitute").line() end, desc = "Substitute line" },
          ["<leader><S-y>"] = { function() require("substitute").eol() end, desc = "Substitute eol" },
        },
        v = {
          ["<leader>y"] = { function() require("substitute").visual() end, desc = "Substitute visual" },
        },
      },
    },
  },
}
