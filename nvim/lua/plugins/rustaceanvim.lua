return {
  "mrcjkb/rustaceanvim",
  dependencies = {
    "AstroNvim/astrocore",
    opts = {
      mappings = {
        n = {
          ["<Leader><Tab>"] = { function() vim.cmd.RustLsp('flyCheck') end, desc = "Rust flycheck" },
        },
      },
    },
  },
}
