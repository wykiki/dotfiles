return {
  "okuuva/auto-save.nvim",
  lazy = false,
  opts = {
    -- debounce_delay = 500,
    trigger_events = {
      defer_save = {},
    },
  },
}
