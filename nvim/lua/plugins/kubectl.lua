return {
  {
    "ramilito/kubectl.nvim",
    dependencies = {
      "AstroNvim/astrocore",
      opts = {
        mappings = {
          n = {
            ["<Leader>k"] = { function() require("kubectl").toggle() end, desc = "Toggle kubectl" },
          },
        },
      },
    },
    config = function()
      require("kubectl").setup()
    end,
  },
}
