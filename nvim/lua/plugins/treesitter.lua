-- if true then return {} end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

-- Customize Treesitter

---@type LazySpec
return {
  "nvim-treesitter/nvim-treesitter",
  build = ":TSUpdate",
	dependencies = {
	  "nushell/tree-sitter-nu",
	},
  opts = {
    auto_install = true,
		highlight = { enable = true },
		indent = { enable = true },
    ensure_installed = {
      "hyprlang",
      "lua",
      "nu",
      "vim",
      "vimdoc",
      -- add more arguments for adding more treesitter parsers
    },
  },
}
