#!/usr/bin/env bash

hyprctl dispatcher renameworkspace 1 Terminal
hyprctl dispatcher renameworkspace 2 Web
hyprctl dispatcher renameworkspace 3 Code
hyprctl dispatcher renameworkspace 4 Perso
hyprctl dispatcher renameworkspace 5 Pro
