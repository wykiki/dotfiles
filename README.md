# Dotconfig

Contains personnal Dotconfig

[TOC]

## Installation

Replace your `~/.config` folder by this one :

```sh
cd ~/.config
git init .
git remote add origin https://gitlab.com/wykiki/dotconfig.git
git checkout main
```
