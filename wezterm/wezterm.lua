local wezterm = require 'wezterm'
local config = {}

config.hide_mouse_cursor_when_typing = false
config.font = wezterm.font 'CaskaydiaCoveNerdFontMono'
config.color_scheme = 'tokyonight_night'

return config
